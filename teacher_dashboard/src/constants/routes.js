export const DASHBOARD_URI = "/";
export const STUDENTS_LIST_URI = "/student-list/:username";
export const PROFILE_URI = "/:username";
