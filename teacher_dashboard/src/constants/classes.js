import tulipe from "../utils/tulipe.jpg";
import marguerite from "../utils/marguerite.jpg";
import hyperium from "../utils/hyperium.jpg";
import rose from "../utils/rose.jpg";

export const classes = {
  marguerite: {
    className: "3ème 3",
    surname: "Marguerite",
    picture: { marguerite },
  },
  tulipe: {
    className: "6ème 1",
    surname: "Tulipe",
    picture: { tulipe },
  },
  rose: {
    className: "4ème 8",
    surname: "Rose",
    picture: { rose },
  },
  hyperium: {
    className: "6ème 7",
    surname: "Hyperium",
    picture: { hyperium },
  },
};
