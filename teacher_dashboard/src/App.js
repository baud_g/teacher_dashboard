import React from "react";
import styled from "styled-components";
import { Route, Switch } from "react-router-dom";
import Home from "./views/Home/Home";
import NavBar from "./views/NavBar/NavBar";
import StudentsList from "./views/StudentsList/StudentsList";
import { DASHBOARD_URI, STUDENTS_LIST_URI } from "./constants/routes";

// #region styled-components
const Container = styled.div`
  margin-right: auto;
  margin-left: auto;
  width: 50%;
  content-align: center;
  text-align: center;
`;
// #endregion

const App = () => (
  <>
    <NavBar />
    <Container>
      <Switch>
        <Route exact path={DASHBOARD_URI} component={Home} />
        <Route exact path={STUDENTS_LIST_URI} component={StudentsList} />
      </Switch>
    </Container>
  </>
);

export default App;
