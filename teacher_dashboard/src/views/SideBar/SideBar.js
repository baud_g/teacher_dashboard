import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Button from "../components/Button";

// #region styled-components
const SideNav = styled.div`
  height: 100%;
  width: ${(props) => (props.on === "true" ? "290px" : "0px")};
  position: fixed;
  z-index: 1;
  top: 0;
  right: 0;
  background-color: #eee;
  overflow-x: hidden;
  padding-top: 60px;
  transition: 0.5s;
`;

const StyledCloseBtn = styled.a`
  position: absolute;
  top: 0;
  left: 15px;
  font-size: 36px;
  margin-right: 50px;
  cursor: pointer;
  &:hover {
    color: grey;
  }
  &:active {
    background-color: ${(props) => props.color};
    transform: translateY(2px);
  }
`;

const StyledInput = styled.input`
  padding: 5px 5px 5px 15px;
  margin: 10px 4px;
  text-decoration: none;
  font-size: 20px;
  color: black;
  transition: 0.3s;
`;
// #endregion

// #region component
const propTypes = {
  on: PropTypes.string,
  student: PropTypes.object,
  onClose: PropTypes.func.isRequired,
  onValidate: PropTypes.func.isRequired,
  onValueChange: PropTypes.func.isRequired,
  onHandleRemove: PropTypes.func.isRequired,
};

const defaultProps = {
  on: "false",
  student: {
    firstName: "",
    lastName: "",
    email: "",
    telephone: "",
    adresse: "",
    id: undefined,
  },
};

const SideBar = ({
  on,
  student,
  onClose,
  onValidate,
  onValueChange,
  onHandleRemove,
}) => {
  const [values, setValues] = useState({ ...student });

  useEffect(() => {
    setValues({ ...student });
  }, [student]);

  return (
    <SideNav id="mySidenav" on={on}>
      <StyledCloseBtn onClick={onClose}>&times;</StyledCloseBtn>
      <label>
        Nom
        <StyledInput
          required
          name="lastName"
          value={values.lastName}
          onChange={onValueChange}
        />
      </label>
      <label>
        Prénom
        <StyledInput
          required
          name="firstName"
          value={values.firstName}
          onChange={onValueChange}
        />
      </label>
      <label>
        Email
        <StyledInput
          required
          name="email"
          value={values.email}
          onChange={onValueChange}
        />
      </label>
      <label>
        Télephone
        <StyledInput
          required
          name="telephone"
          value={values.telephone}
          onChange={onValueChange}
        />
      </label>
      <label>
        Adresse
        <StyledInput
          required
          name="adresse"
          value={values.adresse}
          onChange={onValueChange}
        />
      </label>
      <Button color="green" text="Valider" onClick={onValidate} />
      <Button color="red" text="Annuler" onClick={onClose} />
      <Button color="grey" text="Supprimer" onClick={onHandleRemove} />
    </SideNav>
  );
};

SideBar.propTypes = propTypes;
SideBar.defaultProps = defaultProps;
// #endregion

export default SideBar;
