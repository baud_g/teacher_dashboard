import React from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { classes } from "../../constants/classes";
import Card from "../components/Card";

// #region styled-components
const Container = styled.div`
  width: 100%;
`;
// #endregion

// #region components
const Home = () => {
  const history = useHistory();

  return (
    <Container>
      {Object.keys(classes).map((classe) => (
        <Card
          key={classe}
          onClick={() => history.push(`student-list/${classe}`)}
          surname={classes[classe].surname}
          classeName={classes[classe].className}
          picture={classes[classe].picture[classe]}
        />
      ))}
    </Container>
  );
};
// #endregion

export default Home;
