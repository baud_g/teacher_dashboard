import React from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { DASHBOARD_URI } from "../../constants/routes";

// #region styled-components
const Navbar = styled.div`
  background-color: whites;
  height: 10%;
  width: 100%;
  overflow: hidden;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  text-align: center;
`;

const NavButton = styled.a`
  float: left;
  color: grey;
  text-align: center;
  padding: 20px 20px;
  text-decoration: none;
  font-size: 17px;
  cursor: pointer;
  &:hover {
    background-color: #ddd;
    color: black;
  }
  &:active {
    background-color: lightgreen;
    color: white;
  }
`;
// #endregion

// #region components
const NavBar = () => {
  const history = useHistory();
  return (
    <Navbar>
      <NavButton onClick={() => history.push(DASHBOARD_URI)}>Home</NavButton>
    </Navbar>
  );
};

// #endregion;

export default NavBar;
