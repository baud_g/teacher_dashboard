import React, { useState } from "react";
import styled from "styled-components";
import { useParams } from "react-router-dom";
import { classes } from "../../constants/classes";
import { Students } from "../../constants/student";
import SideBar from "../SideBar/SideBar";
import Button from "../components/Button";

// #region styled-components
const StyledTable = styled.table`
  border: 2px solid rgb(200, 200, 200);
  margin-top: 30px;
  border-collapse: collapse;
  width: 100%;
  font-family: sans-serif;

  caption {
    padding-bottom: 10px;
  }

  td {
    padding: 10px 10px;
  }

  thead > tr > th {
    padding: 10px 10px;
  }

  tbody > tr {
    cursor: pointer;
    :nth-of-type(odd) {
      background-color: #efefef;
    }
    :hover {
      background-color: lightgreen;
    }
    margin: 30px;
  }

  tbody > tr > th {
    color: #7da9f5;
  }
`;
// #endregion

const initialStudent = {
  firstName: "",
  lastName: "",
  email: "",
  telephone: "",
  adresse: "",
  id: -1,
};

// #region components
const StudentsList = () => {
  const { username } = useParams();
  const className = classes[username].className;
  const [validateType, setValidateType] = useState();
  const [students, setStudents] = useState(
    Students.filter((student) =>
      student.class.includes(className)
    ).sort((a, b) => (a.lastName > b.lastName ? 1 : -1))
  );

  const [student, setStudent] = useState();
  const [open, setOpen] = useState("false");
  const [id, setId] = useState();

  const openSideNav = (student, type, id) => {
    if (type === "new") {
      setStudent(initialStudent);
    } else {
      setStudent(student);
    }
    setId(id);
    setValidateType(type);
    setOpen("true");
  };

  const closeSideNav = () => {
    setStudent(initialStudent);
    setValidateType("");
    setOpen("false");
  };

  const onValidate = () => {
    let newArr = students;
    if (validateType === "edit") {
      for (var i in students) {
        if (students[i].id === id) {
          newArr[i].firstName = student.firstName;
          newArr[i].lastName = student.lastName;
          newArr[i].email = student.email;
          newArr[i].telephone = student.telephone;
          newArr[i].adresse = student.adresse;
          newArr[i].id = student.id;
          setStudents(
            newArr.sort((a, b) =>
              a.lastName.toLowerCase() > b.lastName.toLowerCase() ? 1 : -1
            )
          );
          break;
        }
      }
    } else if (validateType === "new") {
      newArr.push({ ...student, id: Students.length + 1 });
      setStudents(
        newArr.sort((a, b) =>
          a.lastName.toLowerCase() > b.lastName.toLowerCase() ? 1 : -1
        )
      );
    }
    setOpen("false");
    setStudent(initialStudent);
  };

  const onValueChange = (event) => {
    const { target } = event;
    setStudent((currentValues) => ({
      ...currentValues,
      [target.name]: target.value,
    }));
  };

  const onHandleRemoveStudent = () => {
    setStudents((students) =>
      students.filter((stud) => stud.id !== student.id)
    );
    setOpen("false");
    setStudent({
      firstName: "",
      lastName: "",
      email: "",
      telephone: "",
      adresse: "",
      id: -1,
    });
  };

  return (
    <>
      <SideBar
        on={open}
        student={student}
        onClose={closeSideNav}
        onValidate={onValidate}
        onValueChange={onValueChange}
        onHandleRemove={onHandleRemoveStudent}
      />
      <StyledTable>
        <caption>
          Liste des élèves de la classe{" "}
          {username.charAt(0).toUpperCase() + username.slice(1)}
        </caption>
        <thead>
          <tr>
            <th scope="col">Nom</th>
            <th scope="col">Prénom</th>
            <th scope="col">Email</th>
            <th scope="col">Téléphone</th>
            <th scope="col">Adresse</th>
          </tr>
        </thead>
        <tbody>
          {students.map((student) => (
            <tr
              key={student.id}
              onClick={() => openSideNav(student, "edit", student.id)}
            >
              <th scope="row">{student.lastName}</th>
              <td>{student.firstName}</td>
              <td>{student.email}</td>
              <td>{student.telephone}</td>
              <td>{student.adresse}</td>
            </tr>
          ))}
        </tbody>
      </StyledTable>
      <Button
        text="Ajouter"
        color="blue"
        onClick={() => openSideNav({}, "new")}
      />
    </>
  );
};
// #endregion

export default StudentsList;
