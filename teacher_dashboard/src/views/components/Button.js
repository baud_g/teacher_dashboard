import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

// #region styled-components
const StyledButton = styled.button`
  margin: 4px 20px;
  background-color: white;
  border-radius: 12px;
  border: 2px solid ${(props) => props.color};
  font-size: 20px;
  color: ${(props) => props.color};
  cursor: pointer;
  padding: 5px 10px;
  box-shadow: -3px 5px #999;
  outline: none;
  &:hover {
    background-color: ${(props) => props.color};
    color: white;
  }
  &:active {
    background-color: ${(props) => props.color};
    box-shadow: -1px 4px #666;
    transform: translateY(4px);
  }
`;
// #endregion

// #region components
const propTypes = {
  color: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

const defaultProps = {
  color: "grey",
  onClick: undefined,
};

const Button = ({ color, text, onClick }) => {
  return (
    <StyledButton color={color} onClick={onClick}>
      {text}
    </StyledButton>
  );
};

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;
// #endregion

export default Button;
