import React from "react";
import styled from "styled-components";

// #region styled-components
const Container = styled.div`
  float: left;
  width: 12em;
  height: 20em;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  text-align: center;
  margin: 5%;
  cursor: pointer;
  &:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
  }
  &:active {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.3);
    transform: translateY(2px);
  }
`;

const ImgContainer = styled.img`
  width: 100%;
  height: 65%;
`;

const DetailsContainer = styled.div`
  padding: 0px 16px;
`;
// #endregion

// #region components
const Card = ({ surname, classeName, picture, onClick }) => {
  return (
    <Container onClick={onClick}>
      <ImgContainer src={picture} />
      <DetailsContainer>
        <h4>
          <b>{surname}</b>
        </h4>
        {classeName}
      </DetailsContainer>
    </Container>
  );
};
// #endregion

export default Card;
